package org.empresa.nomina.service;

import org.empresa.nomina.repository.HolidayRepository;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Service
public class HolidayService {

    private final HolidayRepository holidayRepository;


    public HolidayService(HolidayRepository holidayRepository) {
        this.holidayRepository = holidayRepository;
    }

    public String getNextPayday(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate inputDate;
        try {
            inputDate = LocalDate.parse(date, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Invalid date format. Expected yyyy-MM-dd");
        }
        if (inputDate.getDayOfMonth() <= 15) {
            return dateNominaHolidayQuince(inputDate);
        } else {
            return dateNominaHolidayTreinta(inputDate);
        }


    }

    private String dateNominaHolidayQuince(LocalDate date) {
        LocalDate quince = date.withDayOfMonth(15);
        return getDateString(quince);
    }

    private LocalDate getDateHoliDay(LocalDate date) {
        while (isHoliday(date.toString())) {
            int daysToSubtract = date.getDayOfWeek() == DayOfWeek.MONDAY ? 3 : 1;
            date = date.minusDays(daysToSubtract);
        }
        return date;
    }


    private String dateNominaHolidayTreinta(LocalDate date) {
        LocalDate treinta = date.withDayOfMonth(30);
        return getDateString(treinta);
    }

    private String getDateString(LocalDate dateIn) {
        dateIn = getDateHoliDay(dateIn);
        if (dateIn.getDayOfWeek() == DayOfWeek.SATURDAY) {
            dateIn = dateIn.minusDays(1);
            dateIn = getDateHoliDay(dateIn);
        } else if (dateIn.getDayOfWeek() == DayOfWeek.SUNDAY) {
            dateIn = dateIn.minusDays(2);
            dateIn = getDateHoliDay(dateIn);
        }
        return dateIn.toString();
    }


    private boolean isHoliday(String date) {
        return holidayRepository.existsByDate(date);
    }
}