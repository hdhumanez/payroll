package org.empresa.nomina.controller;

import org.empresa.nomina.controller.dto.Response;
import org.empresa.nomina.service.HolidayService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/nomina")
public class NominaController {
    private final HolidayService holidayService;

    public NominaController(HolidayService holidayService) {
        this.holidayService = holidayService;
    }


    @GetMapping("/next-payday")
    public ResponseEntity<Response> getDateNomina(@RequestParam String date) {
        var dateResponse = holidayService.getNextPayday(date);
        return ResponseEntity.ok(new Response(dateResponse, 200));
    }
}
