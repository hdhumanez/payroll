package org.empresa.nomina.repository;


import org.empresa.nomina.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HolidayRepository extends JpaRepository<Holiday, String> {
    boolean existsByDate(String date);
}